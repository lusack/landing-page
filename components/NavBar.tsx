import React, { useContext } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Logo from './Logo';
import Grid from '@material-ui/core/Grid';
import { useTheme } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { AuthContext } from '../src/auth';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import List from '@material-ui/core/List';
import Drawer from '@material-ui/core/Drawer';
import MenuIcon from '@material-ui/icons/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const buttonPadding = { margin: '0 .8rem 0 .8rem' };

const NavBar = () => {
    let loggedIn: boolean;
    try {
        const { currentUser } = useContext(AuthContext);
        if (currentUser === null) {
            loggedIn = false;
        } else {
            loggedIn = true;
        }
    } catch {
        loggedIn = false;
    }

    const matches = useMediaQuery('(min-width:1040px)');

    function TemporaryDrawer() {
        const [drawerOpen, setdrawerOpen] = React.useState(false);

        const handleDrawerToggle = () => {
            setdrawerOpen(!drawerOpen);
        };

        const drawer = (
            <div>
                <div />
                <Divider />
                <List>
                    <div>
                        {
                            //you can use mapping for this
                        }
                        <Divider />
                        <ListItem button key="FEATURES">
                            <ListItemText
                                style={{ padding: '1rem 4rem 1rem 4rem' }}
                                primary={
                                    <Typography variant="h4">
                                        FEATURES
                                    </Typography>
                                }
                            />
                        </ListItem>
                        <Divider />
                        <ListItem button key="PRICING">
                            <ListItemText
                                style={{ padding: '1rem 4rem 1rem 4rem' }}
                                primary={
                                    <Typography variant="h4">
                                        PRICING
                                    </Typography>
                                }
                            />
                        </ListItem>
                        <Divider />
                        <ListItem button key="FAQ">
                            <ListItemText
                                style={{ padding: '1rem 4rem 1rem 4rem' }}
                                primary={
                                    <Typography variant="h4">FAQ</Typography>
                                }
                            />
                        </ListItem>
                        <Divider />
                        <ListItem button key="SIGNUP">
                            <ListItemText
                                style={{ padding: '1rem 4rem 1rem 4rem' }}
                                primary={
                                    <Typography variant="h4">
                                        SIGN UP
                                    </Typography>
                                }
                            />
                        </ListItem>
                    </div>
                </List>
            </div>
        );

        return (
            <div>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                    onClick={handleDrawerToggle}
                >
                    <MenuIcon />
                </IconButton>

                <Drawer
                    variant="temporary"
                    anchor="right"
                    open={drawerOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true,
                    }}
                >
                    {drawer}
                </Drawer>
            </div>
        );
    }

    function ResponsiveHamburger() {
        if (matches) {
            return (
                <Grid
                    container
                    xs={8}
                    lg={12}
                    item
                    direction="row"
                    alignItems="center"
                    justify="flex-end"
                    wrap="nowrap"
                    spacing={3}
                >
                    <Grid item>
                        <Button color="primary" style={buttonPadding}>
                            Features
                        </Button>
                    </Grid>

                    <Grid item>
                        <Button href="#" color="primary" style={buttonPadding}>
                            Pricing
                        </Button>
                    </Grid>

                    <Grid item>
                        <Button href="#" color="primary" style={buttonPadding}>
                            FAQ
                        </Button>
                    </Grid>

                    <Grid item>{loggedIn ? isLoggedIn() : notLoggedIn()}</Grid>
                </Grid>
            );
        } else {
            return (
                <Grid
                    container
                    xs={11}
                    lg={11}
                    item
                    direction="row"
                    alignItems="center"
                    justify="flex-end"
                    wrap="nowrap"
                    spacing={1}
                >
                    <Grid item>
                        <TemporaryDrawer />
                    </Grid>
                </Grid>
            );
        }
    }

    const theme = useTheme();

    return (
        <AppBar
            style={{ backgroundColor: theme.palette.secondary.contrastText }}
            position="sticky"
        >
            <Grid
                style={{ padding: '0 5rem 0 5rem' }}
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                spacing={3}
            >
                <Grid item xs={1} lg={1}>
                    <Link href="/">
                        <Button>
                            <Logo />
                        </Button>
                    </Link>
                </Grid>
                {/* <Grid item xs={4}/> */}
                <Grid
                    xs={11}
                    lg={11}
                    item
                    container
                    direction="row"
                    alignItems="center"
                    justify="flex-end"
                    wrap="nowrap"
                    spacing={3}
                >
                    <ResponsiveHamburger />
                </Grid>
            </Grid>
        </AppBar>
    );
};

const notLoggedIn = () => {
    return (
        <Link href="/signup" underline="none">
            <Button variant="outlined" color="primary" style={buttonPadding}>
                Sign Up
            </Button>
        </Link>
    );
};
const isLoggedIn = () => {
    return <Avatar alt="Avatar" />;
};

export default NavBar;
