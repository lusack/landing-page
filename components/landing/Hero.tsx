import React, { ReactElement } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import theme from '../../themes/theme';
import HeroMockup from '../HeroMockup';
import Button from '@material-ui/core/Button';

interface Props {}

export default function Hero({}: Props): ReactElement {
    return (
        <div>
            <div
                style={{
                    padding: '3rem 0 0 0 ',
                    height: '100vh',
                    // background:
                    //     'linear-gradient(180.06deg, #171E27 59.22%, #5A2A35 105.91%)',
                }}
            >
                <Grid
                    container
                    alignContent="center"
                    direction="column"
                    justify="center"
                    spacing={3}
                    style={{ textAlign: 'center' }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h1">
                            A PC Tailored For{' '}
                            <span
                                style={{
                                    color: theme.palette.primary.main,
                                }}
                            >
                                You
                            </span>
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="body1">
                            Having trouble building the perfect PC? We got you
                            covered.
                        </Typography>
                    </Grid>

                    <Grid
                        xs={12}
                        item
                        container
                        direction="column"
                        alignContent="center"
                        justify="center"
                        style={{
                            position: 'relative',
                            padding: '3rem 0 0 0',
                        }}
                    >
                        <Grid
                            item
                            xs={12}
                            container
                            direction="column"
                            alignContent="center"
                            justify="center"
                            style={{ width: '100%', height: '100%' }}
                        >
                            <HeroMockup width="40%" />
                        </Grid>
                        <Grid item xs={12}>
                            <Button
                                variant="contained"
                                color="primary"
                                href="/signup"
                                style={{
                                    margin: '4rem 0 0 0',
                                    padding: '.8rem 3rem .8rem 3rem',
                                }}
                            >
                                Get Started
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}></Grid>
                </Grid>
            </div>
        </div>
    );
}
