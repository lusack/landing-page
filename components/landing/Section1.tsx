import React, { ReactElement } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import theme from '../../themes/theme';
import Mockup2 from '../Mockup2';

interface Props {}

function Section1({}: Props): ReactElement {
    return (
        <div
            style={{
                height: '100vh',
                backgroundColor: theme.palette.primary.main,
                // background:
                //     'linear-gradient(180deg, #5A2A35 0%, #DB434E 60.42%, #5A2A35 100%)',
            }}
        >
            <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                style={{ width: '100%', height: '100%' }}
            >
                <Grid
                    item
                    container
                    direction="column"
                    alignItems="flex-start"
                    spacing={4}
                    xs={6}
                    style={{ padding: '0 3rem 10rem 5rem' }}
                >
                    <Grid item xs={10}>
                        <Typography variant="h1">
                            generate your pc based on the games you play
                        </Typography>
                    </Grid>

                    <Grid item xs={9} style={{ padding: ' 0 0 0 7rem' }}>
                        <Typography variant="body1">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit ut aliquam, purus sit amet luctus venenatis,
                            lectus magna fringilla urna, porttitor
                        </Typography>
                    </Grid>
                </Grid>
                <Grid
                    item
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    spacing={4}
                    xs={6}
                    style={{ width: '100%', height: '100%' }}
                >
                    <Grid
                        item
                        xs={12}
                        container
                        direction="column"
                        alignContent="center"
                        justify="center"
                        style={{
                            position: 'relative',
                            width: '100%',
                            height: '100%',
                        }}
                    >
                        <Mockup2 width="75%" />
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default Section1;
