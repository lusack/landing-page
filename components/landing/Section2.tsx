import React, { ReactElement } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

interface Props {}

function Section2({}: Props): ReactElement {
    return (
        <div
            style={{
                height: '100vh',
                // background:
                //     'linear-gradient(180deg, #5A2A35 10.99%, #171E27 58.5%)',
            }}
        >
            <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="stretch"
                style={{ width: '100%', height: '100%' }}
            >
                <Grid
                    item
                    container
                    direction="column"
                    alignItems="center"
                    justify="center"
                    spacing={4}
                    xs={6}
                    style={{ margin: '0 0 0 0' }}
                >
                    <Typography variant="h1">Placeholder</Typography>
                </Grid>
                <Grid
                    item
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    xs={6}
                    style={{
                        margin: '0 0 0 0',
                        backgroundColor: 'rgba(0,0,0,.36)',
                    }}
                >
                    <Grid
                        item
                        xs={10}
                        style={{
                            flexGrow: 0,
                            flexBasis: 0,
                            padding: '0 0 0 0',
                        }}
                    >
                        <Typography variant="h1">
                            Choose between the generated builds
                        </Typography>
                    </Grid>
                    <Grid item xs style={{ flexGrow: 0, padding: '0 0 0 0' }}>
                        <Typography variant="h3">
                            to fit your budget, needs, and looks.
                        </Typography>
                    </Grid>
                    <Grid
                        item
                        xs={10}
                        style={{
                            flexGrow: 0,
                            flexBasis: 0,
                            padding: '3rem 0 0 0',
                        }}
                    >
                        <Typography variant="body1">
                            Building a PC takes time and effort. With a few
                            clicks, we can guarantee a build that fits your
                            budget, needs, and looks.
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default Section2;
