import React, { ReactElement } from 'react';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import Typography from '@material-ui/core/Typography';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useTheme } from '@material-ui/core/styles';

interface Props {}
export default function FAQ({}: Props): ReactElement {
    const theme = useTheme();

    return (
        <div
            style={{
                padding: '3rem 0 3rem 0 ',
                width: '100vw',
                backgroundColor: theme.palette.text.primary,
            }}
        >
            <Grid
                container
                alignContent="center"
                direction="column"
                justify="center"
                spacing={5}
                style={{ textAlign: 'center' }}
            >
                <Grid
                    item
                    container
                    justify="center"
                    alignItems="stretch"
                    direction="column"
                    xs={9}
                >
                    <Typography variant="h1" color="textSecondary">
                        FREQUENTLY ASKED QUESTIONS
                    </Typography>
                </Grid>

                <Grid
                    container
                    item
                    justify="center"
                    alignItems="stretch"
                    direction="column"
                    xs={9}
                >
                    <ExpansionPanel>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography variant="body2">
                                How much does Lusack cost?
                            </Typography>
                        </ExpansionPanelSummary>

                        <ExpansionPanelDetails>
                            <Typography variant="body2" align="left">
                                For the set of features listed above, Lusack is
                                completely free. In the future, we will be
                                adding an optional premium option that will
                                include additional features in addition to the
                                features you get for free.
                            </Typography>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>

                    <ExpansionPanel>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel2a-content"
                            id="panel2a-header"
                        >
                            <Typography variant="body2" align="left">
                                Will more websites be supported in the future?
                            </Typography>
                        </ExpansionPanelSummary>

                        <ExpansionPanelDetails>
                            <Typography variant="body2" align="left">
                                Yes! We are currently working on supporting
                                Amazon, Best Buy, and Ebay and are looking into
                                supporting NewEgg, Fry's, Walmart, Microcenter,
                                along with continued improvements for already
                                supported websites.
                            </Typography>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>

                    <ExpansionPanel>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel3a-content"
                            id="panel3a-header"
                        >
                            <Typography variant="body2" align="left">
                                How can I help?
                            </Typography>
                        </ExpansionPanelSummary>

                        <ExpansionPanelDetails>
                            <Typography variant="body2" align="left">
                                We created our app to assist many people with
                                their PC builds. If you enjoy our product or the
                                idea of Lusack, share it with your friends!
                            </Typography>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>

                    <ExpansionPanel>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel4a-content"
                            id="panel4a-header"
                        >
                            <Typography variant="body2" align="left">
                                How can I get in touch?
                            </Typography>
                        </ExpansionPanelSummary>

                        <ExpansionPanelDetails>
                            <Typography variant="body2" align="left">
                                You can contact us at our company email address
                                at Lusack.inc@gmail.com
                            </Typography>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </Grid>
                <Grid
                    direction="row"
                    justify="flex-end"
                    alignItems="baseline"
                    item
                    xs={9}
                    container
                >
                    <Link href="/signup" underline="none">
                        <Button variant="outlined" color="primary">
                            Question not answered? Contact us!
                        </Button>
                    </Link>
                </Grid>
            </Grid>
        </div>
    );
}
