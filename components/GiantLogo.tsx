import React, { ReactElement } from 'react';

interface Props {
    width?: string;
    height?: string;
    xPos?: string;
    yPos?: string;
}

function GiantLogo({ xPos, yPos }: Props): ReactElement {
    return (
        <svg
            width="7380"
            height="7691"
            viewBox={`${xPos} ${yPos} 7380 7691`}
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <g opacity="0.15">
                <path
                    d="M1339.87 3772.86L1293.89 3874.36L3017.03 4655L3164.3 4599.26L1339.87 3772.86Z"
                    fill="#F3F5F3"
                />
                <path
                    d="M5418.5 5742.66L5464.48 5641.16L3572.01 4783L3424.51 4839L5418.5 5742.66Z"
                    fill="#F3F5F3"
                />
                <path
                    d="M2399.53 6557.65L2501.03 6603.63L3228.53 4996L3173.03 4849L2399.53 6557.65Z"
                    fill="#F3F5F3"
                />
                <rect
                    x="3375.19"
                    y="4756.22"
                    width="126.559"
                    height="126.559"
                    transform="rotate(159.369 3375.19 4756.22)"
                    fill="#FF4655"
                />
                <path
                    d="M6039.63 3918.17L6085.61 3816.67L4362.48 3036L4215.21 3091.74L6039.63 3918.17Z"
                    fill="#F3F5F3"
                />
                <path
                    d="M1961.02 1948.31L1915.04 2049.81L3807.5 2908L3955 2852L1961.02 1948.31Z"
                    fill="#F3F5F3"
                />
                <path
                    d="M3357.5 4444.5L3412.99 4592.68L4021 3250.5L3965.5 3103L3357.5 4444.5Z"
                    fill="#F3F5F3"
                />
                <path
                    d="M4980 1133.36L4878.5 1087.38L4150.98 2695L4206.48 2842L4980 1133.36Z"
                    fill="#F3F5F3"
                />
                <rect
                    x="4004.31"
                    y="2934.78"
                    width="126.559"
                    height="126.559"
                    transform="rotate(-20.6301 4004.31 2934.78)"
                    fill="#FF4655"
                />
            </g>
        </svg>
    );
}

export default GiantLogo;
