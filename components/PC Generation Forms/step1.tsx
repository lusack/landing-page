import React, { ReactElement, useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import { FormContext } from '../../context/FormContext';
import { Typography } from '@material-ui/core';

interface Props {}

export default function Step1({}: Props): ReactElement {
    const { formContext, setFormContext } = useContext(FormContext);
    const { parts } = formContext;

    const handleCheckboxChange = (
        
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        if (event.target.checked) {
            setFormContext({
                ...formContext,
                parts: { ...parts, [event.target.name]: event.target.checked },
            });
        } else {
            setFormContext({
                ...formContext,
                parts: {
                    ...parts,
                    [event.target.name]: event.target.checked,
                    [event.target.name + 'ModelNumber']: '',
                },
            });
        }
    };
    const handleTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFormContext({
            ...formContext,
            parts: { ...parts, [event.target.name]: event.target.value },
        });
    };
    return (
        <FormGroup>
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={3}
            >
                <Grid item xs={6} container justify="flex-end">
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="gpu"
                                checked={parts.gpu}
                                onChange={handleCheckboxChange}
                            />
                        }
                        label="GPU"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        required={parts.gpu}
                        disabled={!parts.gpu}
                        id="gpu-model-number"
                        label="Enter UPC Code"
                        placeholder="enter UPC code"
                        variant="outlined"
                        name="gpuModelNumber"
                        value={parts.gpuModelNumber}
                        onChange={handleTextChange}
                    />
                </Grid>
                <Grid item xs={6} container justify="flex-end">
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="cpu"
                                checked={parts.cpu}
                                onChange={handleCheckboxChange}
                            />
                        }
                        label="CPU"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        required={parts.cpu}
                        disabled={!parts.cpu}
                        id="cpu-model-number"
                        label="Enter UPC Code"
                        placeholder="enter UPC code"
                        variant="outlined"
                        name="cpuModelNumber"
                        value={parts.cpuModelNumber}
                        onChange={handleTextChange}
                    />
                </Grid>

                <Grid item xs={6} container justify="flex-end">
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="ram"
                                checked={parts.ram}
                                onChange={handleCheckboxChange}
                            />
                        }
                        label="RAM"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        required={parts.ram}
                        disabled={!parts.ram}
                        id="ram-model-number"
                        label="Enter UPC Code"
                        placeholder="enter UPC code"
                        variant="outlined"
                        name="ramModelNumber"
                        value={parts.ramModelNumber}
                        onChange={handleTextChange}
                    />
                </Grid>

                <Grid item xs={6} container justify="flex-end">
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="mobo"
                                checked={parts.mobo}
                                onChange={handleCheckboxChange}
                            />
                        }
                        label="Motherboard"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        required={parts.mobo}
                        disabled={!parts.mobo}
                        id="mobo-model-number"
                        label="Enter UPC Code"
                        placeholder="enter UPC code"
                        variant="outlined"
                        name="moboModelNumber"
                        value={parts.moboModelNumber}
                        onChange={handleTextChange}
                    />
                </Grid>
                <Grid item xs={6} container justify="flex-end">
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="psu"
                                checked={parts.psu}
                                onChange={handleCheckboxChange}
                            />
                        }
                        label="PSU"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        required={parts.psu}
                        disabled={!parts.psu}
                        id="psu-model-number"
                        label="Enter UPC Code"
                        placeholder="enter UPC code"
                        variant="outlined"
                        name="psuModelNumber"
                        value={parts.psuModelNumber}
                        onChange={handleTextChange}
                    />
                </Grid>

                <Grid item xs={6} container justify="flex-end">
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="storage"
                                checked={parts.storage}
                                onChange={handleCheckboxChange}
                            />
                        }
                        label="Storage"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        required={parts.storage}
                        disabled={!parts.storage}
                        id="storage-model-number"
                        label="Enter UPC Code"
                        placeholder="enter UPC code"
                        variant="outlined"
                        name="storageModelNumber"
                        value={parts.storageModelNumber}
                        onChange={handleTextChange}
                    />
                </Grid>
                <Grid item>
                    <Typography variant="body1">
                        Press "NEXT" if you don't have an existing parts list
                    </Typography>
                </Grid>
            </Grid>
        </FormGroup>
    );
}
