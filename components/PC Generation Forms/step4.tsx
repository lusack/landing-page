import React, { useContext, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { FormContext } from '../../context/FormContext';
import { ErrorContext } from '../../context/ErrorContext';
import Grid from '@material-ui/core/Grid';
import Error from './error';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(3),
    },
    button: {
        margin: theme.spacing(1, 1, 0, 0),
    },
}));

export default function Step4() {
    const classes = useStyles();

    const { formContext, setFormContext } = useContext(FormContext);
    const { preferences } = formContext;

    const { errorContext, setErrorContext } = useContext(ErrorContext);
    const { error } = errorContext;

    const handleRadioChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFormContext({
            ...formContext,
            preferences: {
                shopping: (event.target as HTMLInputElement).value,
            },
        });
    };
    useEffect(() => {
        if (preferences.shopping === '') {
            setErrorContext({
                error: true,
            });
        } else {
            setErrorContext({
                error: false,
            });
        }
    }, [formContext]);

    return (
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            spacing={3}
        >
            <Grid item>
                <form>
                    <FormControl
                        component="fieldset"
                        className={classes.formControl}
                    >
                        <RadioGroup
                            aria-label="shopping-preference"
                            name="shoppingPreference"
                            value={preferences.shopping}
                            onChange={handleRadioChange}
                        >
                            <FormControlLabel
                                value="new"
                                control={<Radio />}
                                label="Only New"
                            />
                            <FormControlLabel
                                value="used"
                                control={<Radio />}
                                label="Only Used"
                            />
                            <FormControlLabel
                                value="both"
                                control={<Radio />}
                                label="Both New and Used"
                            />
                        </RadioGroup>
                    </FormControl>
                </form>
            </Grid>
            <Grid item>
                <Error err={error} message="Please select a choice." />
            </Grid>
        </Grid>
    );
}
