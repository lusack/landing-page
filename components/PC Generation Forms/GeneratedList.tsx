import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

export default function GeneratedList() {
    return (
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            spacing={3}
        >
            <Grid item>
                <Typography variant="h1" color="primary">
                    Generating Your PC...
                </Typography>
            </Grid>
            <Grid item>
                <CircularProgress color="primary" />
            </Grid>
        </Grid>
    );
}
