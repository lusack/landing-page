import React, { ReactElement, useContext, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { FormContext } from '../../context/FormContext';
import Error from './error';
import { ErrorContext } from '../../context/ErrorContext';

interface Props {}

export default function Step3({}: Props): ReactElement {
    const { formContext, setFormContext } = useContext(FormContext);
    const { purpose } = formContext;

    const { errorContext, setErrorContext } = useContext(ErrorContext);
    const { error } = errorContext;

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFormContext({
            ...formContext,
            purpose: {
                ...purpose,
                [event.target.name]: event.target.checked,
            },
        });
    };
    useEffect(() => {
        let allFalse = true;
        for (const property in purpose) {
            if (purpose[property]) {
                allFalse = false;
            }
        }
        if (allFalse) {
            setErrorContext({
                error: true,
            });
        } else {
            setErrorContext({
                error: false,
            });
        }
    }, [formContext]);

    return (
        <FormGroup>
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                spacing={3}
            >
                <Grid
                    item
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Grid item xs={6} container justify="flex-start">
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="gaming"
                                    checked={purpose.gaming}
                                    onChange={handleChange}
                                />
                            }
                            label="Gaming"
                        />
                    </Grid>

                    <Grid item xs={6} container justify="flex-start">
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="schoolAndWork"
                                    checked={purpose.schoolAndWork}
                                    onChange={handleChange}
                                />
                            }
                            label="School/Work"
                        />
                    </Grid>

                    <Grid item xs={6} container justify="flex-start">
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="cad"
                                    checked={purpose.cad}
                                    onChange={handleChange}
                                />
                            }
                            label="CAD"
                        />
                    </Grid>

                    <Grid item xs={6} container justify="flex-start">
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="videoEditing"
                                    checked={purpose.videoEditing}
                                    onChange={handleChange}
                                />
                            }
                            label="Video Editing"
                        />
                    </Grid>

                    <Grid item xs={6} container justify="flex-start">
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="threeD"
                                    checked={purpose.threeD}
                                    onChange={handleChange}
                                />
                            }
                            label="3-D Applications"
                        />
                    </Grid>

                    <Grid item xs={6} container justify="flex-start">
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="home"
                                    checked={purpose.home}
                                    onChange={handleChange}
                                />
                            }
                            label="Home Use"
                        />
                    </Grid>
                </Grid>
                <Grid item>
                    <Error err={error} message="Please select a choice." />
                </Grid>
            </Grid>
        </FormGroup>
    );
}
