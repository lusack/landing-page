import React, { ReactElement, useContext, useEffect } from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import { FormContext } from '../../context/FormContext';
import Error from './error';
import { ErrorContext } from '../../context/ErrorContext';

interface Props {}

export default function Step2({}: Props): ReactElement {
    const { formContext, setFormContext } = useContext(FormContext);
    const { budget } = formContext;
    const { errorContext, setErrorContext } = useContext(ErrorContext);
    const { error } = errorContext;

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFormContext({
            ...formContext,
            budget: {
                ...budget,
                [event.target.name]: parseFloat(event.target.value),
            },
        });
    };
    useEffect(() => {
        if (
            budget.lower > budget.upper ||
            budget.upper <= 0 ||
            budget.lower < 0
        ) {
            setErrorContext({ error: true });
        } else {
            setErrorContext({ error: false });
        }
    }, [formContext]);
    return (
        <FormGroup>
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                spacing={3}
            >
                <Grid
                    item
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={3}
                >
                    <Grid item>
                        <TextField
                            required={true}
                            id="lower-budget-bound"
                            label="Lower Budget Boundary"
                            placeholder="Lower Budget"
                            variant="outlined"
                            name="lower"
                            onChange={handleChange}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        $
                                    </InputAdornment>
                                ),
                            }}
                            type="number"
                        />
                    </Grid>
                    <Grid item>
                        <Typography variant="h4">-</Typography>
                    </Grid>
                    <Grid item>
                        <TextField
                            required={true}
                            id="upper-budget-bound"
                            label="Upper Budget Boundary"
                            placeholder="Enter Upper Budget"
                            variant="outlined"
                            name="upper"
                            onChange={handleChange}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        $
                                    </InputAdornment>
                                ),
                            }}
                            type="number"
                        />
                    </Grid>
                </Grid>
                <Grid item>
                    <Error err={error} message="Enter a Valid Budget" />
                </Grid>
            </Grid>
        </FormGroup>
    );
}
