import Typography from '@material-ui/core/Typography';
import React, { ReactElement } from 'react';

interface Ierr {
    err: boolean;
    message: string;
}

export default function Error({ err, message }: Ierr): ReactElement {
    if (err) {
        return (
            <Typography variant="body1" color="primary">
                {message}
            </Typography>
        );
    }
    return <div></div>;
}
