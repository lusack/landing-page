import React, { ReactElement } from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Logo from './Logo';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import TwitterIcon from './TwitterIcon';
import FacebookIcon from './FacebookIcon';
import InstagramIcon from './InstagramIcon';
import LinkedinIcon from './LinkedinIcon';

interface Props {}

function Footer({}: Props): ReactElement {
    return (
        <div
            style={{
                width: '100vw',
                backgroundColor: '#11161D',
            }}
        >
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                style={{ padding: '2rem 0 0rem 0' }}
            >
                <Grid
                    item
                    container
                    direction="row"
                    justify="center"
                    alignItems="baseline"
                    xs={12}
                    style={{ padding: '0rem 0 2rem 0' }}
                >
                    <Grid
                        item
                        xs={3}
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item>
                            <Link href="/">
                                <Button>
                                    <Logo />
                                </Button>
                            </Link>
                        </Grid>
                    </Grid>
                    <Grid
                        item
                        xs={3}
                        container
                        direction="column"
                        justify="center"
                        alignItems="flex-start"
                        spacing={1}
                    >
                        <Grid item>
                            <Typography variant="h5" color="primary">
                                Information
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                <Link href="/" color="textPrimary">
                                    Features
                                </Link>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                <Link href="/" color="textPrimary">
                                    Pricing
                                </Link>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                <Link href="/" color="textPrimary">
                                    FAQ
                                </Link>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                <Link href="/" color="textPrimary">
                                    How it Works
                                </Link>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid
                        item
                        xs={3}
                        container
                        direction="column"
                        justify="center"
                        alignItems="flex-start"
                        spacing={1}
                    >
                        <Grid item>
                            <Typography variant="h5" color="primary">
                                Contact Us
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                Email: <br />
                                lusack.inc@gmail.com
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                Phone: <br />
                                +667 231 8446
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid
                        item
                        xs={3}
                        container
                        direction="column"
                        justify="center"
                        alignItems="flex-start"
                        spacing={1}
                    >
                        <Grid item>
                            <Typography variant="h5" color="primary">
                                Helpful Links
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2" color="textPrimary">
                                <Link href="#" color="textPrimary">
                                    Terms of Service
                                </Link>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2" color="textPrimary">
                                <Link href="#" color="textPrimary">
                                    Privacy Policy
                                </Link>
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid
                    item
                    container
                    justify="center"
                    alignItems="center"
                    xs={12}
                    style={{ padding: '0rem 0 4rem 0' }}
                >
                    <Grid item>
                        <Typography variant="body2">
                            © 2020 Lusack USA, Inc. All rights reserved
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Button>
                            <LinkedinIcon width="1.5rem" height="auto" />
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button>
                            <InstagramIcon width="1.5rem" height="auto" />
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button>
                            <FacebookIcon width="1.5rem" height="auto" />
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button>
                            <TwitterIcon width="1.5rem" height="auto" />
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default Footer;
