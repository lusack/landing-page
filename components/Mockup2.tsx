import React, { ReactElement } from 'react';

interface Props {
    width?: string;
    height?: string;
}

export default function Mockup2({ width }: Props): ReactElement {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox={`0 0 679 445`}
            width={width}
        >
            <rect width="679" height="445" fill="#040f1c" rx="5"></rect>
            <path
                fill="#1d2632"
                d="M0 5a5 5 0 015-5h669a5 5 0 015 5v21.9H0z"
            ></path>
            <path
                fill="#fe4a57"
                d="M59.2 182v26h-3.5v-26zm39.7 27.2l-6-15.6-6.7 15.6-10.4-27.1h4.1l6.7 18.9 4.5-11.8-2.7-7.1h3.4l7.3 18.5 6.4-18.5h3.7zm25.9-8h-10L112 208h-3.5l11.4-27.1h.2l11.4 27.1h-4zm-1.2-3.1l-3.7-9.5-3.9 9.5zM159 182v26.9h-.1l-19.1-19.4V208h-3.5v-26.8h.1l19.1 19.6V182zm22.9 0v3.3h-7V208h-3.5v-22.7h-6.8V182zm32.3 0v3.3h-6.9V208h-3.5v-22.7H197V182zm2.9 13a12.6 12.6 0 011.8-6.6 13.3 13.3 0 014.8-4.9 12.9 12.9 0 016.7-1.8 13.1 13.1 0 016.6 1.8 13.3 13.3 0 014.8 4.9 11.8 11.8 0 011.9 6.6 13.2 13.2 0 01-1.8 6.7 14.1 14.1 0 01-4.9 4.8 13.1 13.1 0 01-6.6 1.8 12.9 12.9 0 01-6.7-1.8 13.3 13.3 0 01-4.9-4.8 13.6 13.6 0 01-1.7-6.7zm3.6 0a9.6 9.6 0 001.3 5 9.3 9.3 0 003.5 3.6 9.5 9.5 0 004.9 1.3 9.2 9.2 0 004.9-1.3 10.1 10.1 0 003.5-3.6 10.3 10.3 0 000-10 11 11 0 00-3.5-3.6 9.6 9.6 0 00-4.9-1.4 9.1 9.1 0 00-4.9 1.4 10.3 10.3 0 00-3.6 3.6 10.8 10.8 0 00-1.2 5zm47.2-13a9.8 9.8 0 016.7 2.1 7.5 7.5 0 012.4 5.9 9.1 9.1 0 01-.9 4.1 7.2 7.2 0 01-2.9 3.1 9.1 9.1 0 01-4.9 1.2h-3.9v9.6H261v-26zm.4 13a4.6 4.6 0 003-.8 3.9 3.9 0 001.7-1.9 5.9 5.9 0 00.5-2.2 5.2 5.2 0 00-1.2-3.3 4.6 4.6 0 00-3.8-1.5h-4.1v9.7zm17.6-13v22.6h13.2v3.4h-16.7v-26zm32.5 19.2h-10l-2.8 6.8h-3.4l11.3-27.1h.3l11.3 27.1h-4zm-1.2-3.1l-3.7-9.5-3.8 9.5zm29.7-16.1l-9.8 17.3v8.7h-3.5v-8.7L324 182h4.3l7.2 13.4 7.1-13.4z"
            ></path>
            <path
                fill="#ff4655"
                d="M118.1 257a12.6 12.6 0 011.8-6.6 13.1 13.1 0 0111.5-6.7 12.6 12.6 0 016.6 1.8 12.8 12.8 0 014.9 4.9 12.6 12.6 0 011.8 6.6 13.1 13.1 0 01-6.7 11.5 12.6 12.6 0 01-6.6 1.8 13.2 13.2 0 01-6.7-1.8 13 13 0 01-4.8-4.8 12.7 12.7 0 01-1.8-6.7zm3.6 0a10.6 10.6 0 001.3 5 11 11 0 003.5 3.6 9.8 9.8 0 005 1.3 8.6 8.6 0 004.8-1.3 9.3 9.3 0 003.5-3.6 10.3 10.3 0 000-10 9.6 9.6 0 00-16.8 0 9.8 9.8 0 00-1.3 5zm51.6-13v26.9h-.1l-19.1-19.4V270h-3.5v-26.8h.2l19.1 19.6V244zM418.2 246.6a13.4 13.4 0 00-3-1.2 10.5 10.5 0 00-3-.5 5.3 5.3 0 00-3.4 1 3.1 3.1 0 00-1.2 2.6 3.2 3.2 0 001.5 2.7 22.4 22.4 0 004.1 2 12 12 0 013.4 1.6 5.7 5.7 0 012.3 2.3 6.3 6.3 0 011 3.8 7.3 7.3 0 01-1.1 3.8 7 7 0 01-3 2.6 8.8 8.8 0 01-4.5 1 14.4 14.4 0 01-4.6-.8 17.7 17.7 0 01-3.9-1.9l1.5-2.8a13.8 13.8 0 003.2 1.7 9.5 9.5 0 003.5.7 6.1 6.1 0 003.6-1.1 3.5 3.5 0 001.6-3.2 3.3 3.3 0 00-1.3-2.8 13.9 13.9 0 00-3.7-2.1 17.5 17.5 0 01-3.6-1.6 7.2 7.2 0 01-2.5-2.1 5.8 5.8 0 01-1.1-3.5 6.4 6.4 0 012.1-5 9.3 9.3 0 015.6-2 15.2 15.2 0 017.8 2.1zm24.4-4.6v3.3h-13.4v7.9h12v3.4h-12v8h13.9v3.4h-17.4v-26zm22 0v3.3h-7V268h-3.5v-22.7h-6.8V242zm20.9 0v3.3h-6.9V268h-3.5v-22.7h-6.8V242zm9.1 0v26h-3.5v-26zm30.2 0v26.9h-.1l-19.2-19.4V268H502v-26.8h.2l19.1 19.6V242zm27.9 23.8a12.4 12.4 0 01-3.8 1.7 16.5 16.5 0 01-4.7.8 14.6 14.6 0 01-7.1-1.7 11.2 11.2 0 01-4.8-4.6 12.9 12.9 0 01-1.7-6.5 14.5 14.5 0 011.8-7.4 12.5 12.5 0 014.9-4.8 13.8 13.8 0 016.7-1.6 17.7 17.7 0 014.1.5 16 16 0 013.5 1.4l-1.1 3.2a16 16 0 00-2.9-1.1 9.6 9.6 0 00-3.2-.5 11.2 11.2 0 00-5.3 1.2 8.1 8.1 0 00-3.6 3.5 11.2 11.2 0 00-1.3 5.3 10.3 10.3 0 001.3 5 7.9 7.9 0 003.6 3.4 10.8 10.8 0 005.2 1.2 12.4 12.4 0 002.7-.3 5.8 5.8 0 002.1-.7v-5.2H544v-3.3h8.7zm20.1-19.2a13.4 13.4 0 00-3-1.2 11.1 11.1 0 00-3-.5 5.3 5.3 0 00-3.4 1 3.1 3.1 0 00-1.2 2.6 3.3 3.3 0 001.4 2.7 28.1 28.1 0 004.2 2 12 12 0 013.4 1.6 6.3 6.3 0 012.3 2.3 7.1 7.1 0 01.9 3.8 7.2 7.2 0 01-1 3.8 7 7 0 01-3 2.6 9.3 9.3 0 01-4.5 1 14.4 14.4 0 01-4.6-.8 21.2 21.2 0 01-4-1.9l1.6-2.8a12 12 0 003.2 1.7 9.1 9.1 0 003.5.7 6.1 6.1 0 003.6-1.1 3.5 3.5 0 001.6-3.2 3.3 3.3 0 00-1.3-2.8 13.9 13.9 0 00-3.7-2.1 18.6 18.6 0 01-3.7-1.6 6.9 6.9 0 01-2.4-2.1 5.8 5.8 0 01-1.1-3.5 6.4 6.4 0 012.1-5 9.3 9.3 0 015.6-2 15.2 15.2 0 017.8 2.1z"
            ></path>
            <path
                fill="#43e998"
                d="M242.2 264h-2v-6.3h-7.5v6.3h-2v-14.5h2v6.3h7.5v-6.3h2zm6.1-14.5V264h-2v-14.5zm15.5 13.3l-2.1.9a8.5 8.5 0 01-2.6.5 7.7 7.7 0 01-4-1 6.3 6.3 0 01-2.6-2.5 6.6 6.6 0 01-1-3.7 8.5 8.5 0 011-4 7 7 0 012.7-2.7 8.2 8.2 0 013.8-.9 9.1 9.1 0 012.3.3l1.9.7-.6 1.8-1.7-.6-1.7-.3a5.6 5.6 0 00-5 2.6 7.3 7.3 0 00-.7 3 6.8 6.8 0 00.7 2.8 6.1 6.1 0 002 1.9 6.8 6.8 0 002.9.6h1.5a3.3 3.3 0 001.2-.5v-2.8h-2.9V257h4.9zm15.1 1.2H277v-6.3h-7.6v6.3h-1.9v-14.5h1.9v6.3h7.6v-6.3h1.9z"
            ></path>
            <path
                fill="#f1f1f2"
                d="M355.5 250.8v4.6a1.7 1.7 0 01-.6 1.2l-11.9 8.9c-.2.2-.4.2-.5 0l-11.7-9.1a1.4 1.4 0 01-.6-1.2v-4.5c0-.2 0-.3.1-.3h.4l12 9.4 12.2-9.2h.3c.2 0 .3.1.3.2z"
            ></path>
            <rect
                width="182"
                height="43"
                x="195.5"
                y="235.5"
                fill="none"
                stroke="#868686"
                rx="4.5"
            ></rect>
            <path
                fill="#f3f5f3"
                d="M391.4 185.4h3.2a1 1 0 01.8.5l9 11.7v-11.8c0-.1 0-.2.1-.2l.2-.2h2.6l.2.2a.3.3 0 01.1.2v16.8c0 .3-.1.3-.3.3h-2.4a1.1 1.1 0 01-.8-.3l-12.9-16.8c0-.1-.1-.1 0-.2zm18.2 17.2l12.8-16.7a1.1 1.1 0 01.9-.4h2.3a.3.3 0 01.3.3v16.9a.3.3 0 01-.3.3h-2.3a1.1 1.1 0 01-.9-.4l-4.3-5.7-4.3 5.7a1.1 1.1 0 01-.9.4h-3.3a.2.2 0 010-.4zm10.6-8.3l2.6 3.3v-6.7zm9.6-8.8h2.6a.3.3 0 01.3.3v14h7.2a.2.2 0 010 .3l-1.8 2.4a.9.9 0 01-.8.4h-7.7a.3.3 0 01-.1-.2v-16.9a.3.3 0 01.1-.2zm11.4 8.7a10.5 10.5 0 01.7-3.6 9.7 9.7 0 011.9-2.9 11.2 11.2 0 012.9-1.9 8.8 8.8 0 017.1.1 8.4 8.4 0 012.9 2 9.7 9.7 0 011.9 2.9 8.4 8.4 0 01.7 3.5 8.6 8.6 0 01-.8 3.6 9.4 9.4 0 01-2 2.9 9.2 9.2 0 01-2.8 1.9 10.5 10.5 0 01-3.6.7 8.5 8.5 0 01-3.5-.8 7.7 7.7 0 01-2.8-1.9 8.1 8.1 0 01-1.9-2.9 8.6 8.6 0 01-.7-3.6zm3.2.1a5.9 5.9 0 00.5 2.3 5.3 5.3 0 001.3 2 8.3 8.3 0 001.8 1.3 5.9 5.9 0 002.3.4 5.4 5.4 0 002.2-.5 5.9 5.9 0 001.8-1.3 5.3 5.3 0 001.3-2 5.9 5.9 0 00.4-2.3 5.9 5.9 0 00-.5-2.3 5.3 5.3 0 00-1.3-2 8.3 8.3 0 00-1.8-1.3 5.8 5.8 0 00-4.5.1 5.9 5.9 0 00-1.8 1.3 7.6 7.6 0 00-1.2 1.9 6.9 6.9 0 00-.5 2.4zm17.7-8.5a.3.3 0 01.1-.2l.2-.2h13.2l.2.2c.1 0 .1.1.1.2v3.6a.8.8 0 01-.2.7l-3.9 5.1 5.6 7.4a.2.2 0 010 .3h-3.3a1.4 1.4 0 01-.9-.3l-5.4-7.2q-.3-.2 0-.3l4.9-6.5h-7.4v14c0 .3-.1.3-.3.3h-2.8v-17.1zm15.8 16.8l12.8-16.7a.9.9 0 01.8-.4h2.4a.3.3 0 01.3.3v16.9a.3.3 0 01-.3.3h-2.4a.9.9 0 01-.8-.4l-4.3-5.7-4.4 5.7a.8.8 0 01-.8.4h-3.4a.4.4 0 01.1-.4zm10.6-8.3l2.5 3.3v-6.7zm9.3 8.4v-16.9a.3.3 0 01.1-.2h2.6a.9.9 0 01.8.4l9 11.7v-11.9a.3.3 0 01.3-.3h2.6a.3.3 0 01.3.3v16.9a.3.3 0 01-.3.3h-2.4a.9.9 0 01-.8-.4l-9-11.7v11.8a.3.3 0 01-.3.3h-2.8a.3.3 0 01-.1-.3zm18.4-16.9a.3.3 0 01.1-.2l.2-.2h10.9a1 1 0 01.8.5l1.9 2.4v.2h-5.8v14c0 .3-.1.3-.3.3h-2.6c-.2 0-.3 0-.3-.3v-14h-4.8v-2.8z"
            ></path>
            <path
                fill="#f1f1f2"
                d="M589 189.8v4.5a1.4 1.4 0 01-.6 1.2l-11.8 9h-.6l-11.6-9.1a1.4 1.4 0 01-.6-1.2v-4.6l.2-.3h.4l12 9.3 12.1-9.2h.4c0 .2.1.3.1.4z"
            ></path>
            <rect
                width="239"
                height="43"
                x="371.5"
                y="173.5"
                fill="none"
                stroke="#868686"
                rx="4.5"
            ></rect>
            <circle cx="32" cy="13" r="7" fill="#2c3745"></circle>
            <circle cx="57" cy="13" r="7" fill="#2c3745"></circle>
            <circle cx="81" cy="13" r="7" fill="#2c3745"></circle>
        </svg>
    );
}
