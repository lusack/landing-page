import React, { useState } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Step1 from '../components/PC Generation Forms/step1';
import Step2 from '../components/PC Generation Forms/step2';
import Step3 from '../components/PC Generation Forms/step3';
import Step4 from '../components/PC Generation Forms/step4';
import { FormContext, IFormContext } from '../context/FormContext';
import { IErrorContext, ErrorContext } from '../context/ErrorContext';
import GeneratedList from '../components/PC Generation Forms/GeneratedList';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
        },
        backButton: {
            marginRight: theme.spacing(1),
        },
        instructions: {
            marginTop: theme.spacing(1),
            marginBottom: theme.spacing(1),
        },
    })
);

function getSteps() {
    return [
        'Existing Parts List',
        'Your Budget',
        'Purpose of PC',
        'Buying Preferences',
    ];
}

function getStepTitle(stepIndex: number) {
    switch (stepIndex) {
        case 0:
            return 'Enter your existing parts list';
        case 1:
            return 'How much are you willing to spend on your PC?';
        case 2:
            return 'What is the primary purpose for your PC?';
        case 3:
            return 'Do you prefer shopping used or new?';
        default:
            return 'Unknown stepIndex';
    }
}
function getStepForm(stepIndex: number) {
    switch (stepIndex) {
        case 0:
            return <Step1 />;
            break;

        case 1:
            return <Step2 />;
            break;

        case 2:
            return <Step3 />;
            break;
        case 3:
            return <Step4 />;
            break;
    }
}

const initialAppContext: IFormContext = {
    parts: {
        gpu: false,
        cpu: false,
        ram: false,
        mobo: false,
        psu: false,
        storage: false,
        gpuModelNumber: '',
        cpuModelNumber: '',
        ramModelNumber: '',
        moboModelNumber: '',
        psuModelNumber: '',
        storageModelNumber: '',
    },
    budget: {
        lower: 0,
        upper: 1000,
    },
    purpose: {
        gaming: true,
        schoolAndWork: false,
        cad: false,
        videoEditing: false,
        threeD: false,
        home: false,
    },
    preferences: {
        shopping: 'new',
    },
};
const initialErrorContext: IErrorContext = {
    error: false,
};

export default function generatePC() {
    const classes = useStyles();
    const [activeStep, setActiveStep] = useState(0);
    const steps = getSteps();
    const [formContext, setFormContext] = useState<IFormContext>(
        initialAppContext
    );
    const [errorContext, setErrorContext] = useState<IErrorContext>(
        initialErrorContext
    );

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    return (
        <FormContext.Provider value={{ formContext, setFormContext }}>
            <ErrorContext.Provider value={{ errorContext, setErrorContext }}>
                <div className={classes.root}>
                    <div>
                        <Stepper
                            activeStep={activeStep}
                            alternativeLabel
                            style={{ backgroundColor: 'transparent' }}
                        >
                            {steps.map((label) => (
                                <Step key={label}>
                                    <StepLabel>{label}</StepLabel>
                                </Step>
                            ))}
                        </Stepper>
                    </div>

                    <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        spacing={5}
                    >
                        {/* The title of the page */}
                        <Grid item>
                            {activeStep === steps.length ? (
                                <GeneratedList />
                            ) : (
                                <Typography variant="h2">
                                    {getStepTitle(activeStep)}
                                </Typography>
                            )}
                        </Grid>
                        <Grid item>{getStepForm(activeStep)}</Grid>

                        <Grid item>
                            <div>
                                <Button
                                    disabled={
                                        activeStep === 0 || errorContext.error
                                    }
                                    onClick={handleBack}
                                    className={classes.backButton}
                                    variant="outlined"
                                >
                                    Back
                                </Button>
                                <Button
                                    variant="outlined"
                                    color="primary"
                                    onClick={handleNext}
                                    className={classes.backButton}
                                    disabled={errorContext.error}
                                >
                                    {activeStep === steps.length - 1
                                        ? 'Finish'
                                        : 'Next'}
                                </Button>
                                {activeStep === steps.length - 1 ? (
                                    <Button
                                        variant="outlined"
                                        onClick={handleReset}
                                    >
                                        Reset
                                    </Button>
                                ) : (
                                    ''
                                )}
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </ErrorContext.Provider>
        </FormContext.Provider>
    );
}
