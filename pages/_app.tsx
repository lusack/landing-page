import { AppProps } from 'next/app';
// import BebasNeueTTF from '../fonts/BebasNeue-Regular.ttf';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from '../themes/theme';
import NavBar from '../components/NavBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import { AuthProvider } from '../src/auth';
import Footer from '../components/Footer';

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <AuthProvider>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <NavBar />
                <div
                    style={{
                        backgroundColor: theme.palette.secondary.contrastText,
                        overflow: 'hidden',
                    }}
                >
                    <Component {...pageProps} />
                </div>
                <Footer />
            </ThemeProvider>
        </AuthProvider>
    );
}

export default MyApp;
