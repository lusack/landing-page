import React, { ReactElement, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import GiantLogo from '../components/GiantLogo';
import FacebookIcon from '../components/FacebookIcon';
import GoogleIcon from '../components/GoogleIcon';
import TwitterIcon from '../components/TwitterIcon';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link/Link';
import app from '../src/base';
import Router from 'next/router';
import Error from '../components/PC Generation Forms/error';
import useMediaQuery from '@material-ui/core/useMediaQuery';
// import PrivateRoute from '../src/PrivateRoute';

interface Props {}

interface IState {
    email: string;
    password: string;
    error: boolean;
}

const initState: IState = {
    email: '',
    password: '',
    error: false,
};

export default function login({}: Props): ReactElement {
    const [state, setState] = useState<IState>(initState);
    const { email, password, error } = state;

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setState({ ...state, [event.target.name]: event.target.value });
    };
    const matches = useMediaQuery('(min-width:1920px)');
    const matches1 = useMediaQuery('(min-width:1500px)');
    const matches2 = useMediaQuery('(min-width:1440px)');
    const matches3 = useMediaQuery('(min-width:1280px)');
    const matches4 = useMediaQuery('(min-width:1152px)');
    function LandingBackground() {
        if (matches) {
            //1920 3000 2400 7413 9461
            return <GiantLogo xPos="2600" yPos="2400" />;
        } else if (matches1) {
            //1500
            return <GiantLogo xPos="2800" yPos="2400" />;
        } else if (matches2) {
            //1440
            return <GiantLogo xPos="2800" yPos="2400" />;
        } else if (matches3) {
            //1280
            return <GiantLogo xPos="3000" yPos="2900" />;
        } else if (matches4) {
            //1152
            return <GiantLogo xPos="3100" yPos="2400" />;
        } else {
            return <GiantLogo xPos="3100" yPos="2400" />;
        }
    }
    return (
        <div
            style={{
                position: 'relative',
                overflow: 'hidden',
                width: '100vw',
            }}
        >
            <span style={{ position: 'absolute', zIndex: 0 }}>
                <LandingBackground />
            </span>
            <Grid
                container
                justify="center"
                alignContent="center"
                style={{
                    overflow: 'hidden',
                    position: 'relative',
                    width: '100%',
                    height: '100%',
                }}
            >
                <Grid
                    item
                    container
                    alignContent="center"
                    direction="column"
                    justify="center"
                    spacing={1}
                    style={{ textAlign: 'center', padding: '3rem 0 3rem 0' }}
                >
                    <Grid item>
                        <Typography variant="h6" color="textPrimary">
                            Log In
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography
                            variant="body1"
                            color="textPrimary"
                            style={{ padding: '0 0 2rem 0' }}
                        >
                            Join the Lusack Community
                        </Typography>
                    </Grid>

                    <form action="">
                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <OutlinedInput
                                required
                                id="standard-basic"
                                placeholder="email"
                                fullWidth
                                name="email"
                                value={email}
                                onChange={handleChange}
                            />
                        </Grid>

                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <OutlinedInput
                                required
                                id="standard-basic"
                                placeholder="Password"
                                type="password"
                                fullWidth
                                name="password"
                                value={password}
                                onChange={handleChange}
                            />
                        </Grid>

                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <Button
                                variant="contained"
                                color="primary"
                                fullWidth
                                onClick={() => {
                                    app.auth()
                                        .signInWithEmailAndPassword(
                                            email,
                                            password
                                        )
                                        .then(() => {
                                            // PrivateRoute();
                                            Router.push('/');
                                        })
                                        .catch(function () {
                                            // Handle Errors here.
                                            // console.log("ksajhsdkajhsd")
                                            setState({ ...state, error: true });
                                            // ...
                                        });
                                }}
                            >
                                Log In
                            </Button>
                        </Grid>
                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <Error
                                err={error}
                                message="Enter valid credentials."
                            />
                        </Grid>
                        <Grid
                            item
                            style={{
                                padding: '0 0 .8rem 0',
                                textAlign: 'center',
                            }}
                        >
                            <Typography variant="body1">
                                &#9473;&#9473;&#9473;&#9473;&#9473;&#9473;&#9473;&nbsp;
                                or&nbsp;&#9473;&#9473;&#9473;&#9473;&#9473;&#9473;&#9473;
                            </Typography>
                            <Typography variant="body1">
                                Continue with
                            </Typography>
                        </Grid>
                        <Grid
                            item
                            container
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Grid item xs>
                                <Button
                                    onClick={() => {
                                        var provider = new app.auth.FacebookAuthProvider();
                                        app.auth()
                                            .signInWithPopup(provider)
                                            .then(function () {
                                                // This gives you a Google Access Token. You can use it to access the Google API.
                                                // var token = result.credential.accessToken;
                                                // The signed-in user info.

                                                // PrivateRoute();
                                                Router.push('/');
                                                // ...
                                            })
                                            .catch(function () {
                                                // Handle Errors here.
                                                // The email of the user's account used.
                                                // The firebase.auth.AuthCredential type that was used.
                                                // ...
                                            });
                                    }}
                                >
                                    <FacebookIcon width="2rem" height="auto" />
                                </Button>
                            </Grid>
                            <Grid item xs>
                                <Button
                                    onClick={() => {
                                        var provider = new app.auth.GoogleAuthProvider();
                                        app.auth()
                                            .signInWithPopup(provider)
                                            .then(function () {
                                                // This gives you a Google Access Token. You can use it to access the Google API.
                                                // var token = result.credential.accessToken;
                                                // The signed-in user info.
                                                // PrivateRoute();
                                                Router.push('/');
                                                // ...
                                            })
                                            .catch(function () {
                                                // Handle Errors here.
                                                // The email of the user's account used.
                                                // The firebase.auth.AuthCredential type that was used.
                                                // ...
                                            });
                                    }}
                                >
                                    <GoogleIcon width="2rem" height="auto" />
                                </Button>
                            </Grid>
                            <Grid item xs>
                                <Button disabled={true}>
                                    <TwitterIcon width="2rem" height="auto" />
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography
                                variant="body2"
                                style={{ padding: '3rem 0 0 0' }}
                            >
                                {'New to Lusack? '}
                                <Link variant="body2" href="/signup">
                                    Sign Up Here
                                </Link>
                            </Typography>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
        </div>
    );
}
