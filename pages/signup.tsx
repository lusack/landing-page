import React, { ReactElement, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import GiantLogo from '../components/GiantLogo';
import FacebookIcon from '../components/FacebookIcon';
import GoogleIcon from '../components/GoogleIcon';
import TwitterIcon from '../components/TwitterIcon';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Link from '@material-ui/core/Link/Link';
import app from '../src/base';
import Error from '../components/PC Generation Forms/error';
import Router from 'next/router';
// import PrivateRoute from '../src/PrivateRoute';
import useMediaQuery from '@material-ui/core/useMediaQuery';

interface IState {
    email: string;
    password: string;
    confirmPass: string;
    error: boolean;
    check: boolean;
    check2: boolean;
}

const initState: IState = {
    email: '',
    password: '',
    confirmPass: '',
    error: false,
    check: false,
    check2: false,
};
export default function signup(): ReactElement {
    const [state, setState] = useState<IState>(initState);
    const { email, password, confirmPass, error, check, check2 } = state;
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setState({ ...state, [event.target.name]: event.target.value });
    };
    const handleCheckboxChange = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        setState({
            ...state,
            check: event.target.checked,
        });
    };
    const handleCheckboxChange2 = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        setState({
            ...state,
            check2: event.target.checked,
        });
    };
    const matches = useMediaQuery('(min-width:1920px)');
    const matches1 = useMediaQuery('(min-width:1500px)');
    const matches2 = useMediaQuery('(min-width:1440px)');
    const matches3 = useMediaQuery('(min-width:1280px)');
    const matches4 = useMediaQuery('(min-width:1152px)');
    function LandingBackground() {
        if (matches) {
            //1920 3000 2400 7413 9461
            return <GiantLogo xPos="2600" yPos="2400" />;
        } else if (matches1) {
            //1500
            return <GiantLogo xPos="2800" yPos="2400" />;
        } else if (matches2) {
            //1440
            return <GiantLogo xPos="2800" yPos="2400" />;
        } else if (matches3) {
            //1280
            return <GiantLogo xPos="3000" yPos="2900" />;
        } else if (matches4) {
            //1152
            return <GiantLogo xPos="3100" yPos="2400" />;
        } else {
            return <GiantLogo xPos="3100" yPos="2400" />;
        }
    }

    return (
        <div
            style={{
                position: 'relative',
                overflow: 'hidden',
                width: '100vw',
            }}
        >
            <span style={{ position: 'absolute', zIndex: 0 }}>
                <LandingBackground />
            </span>
            <Grid
                container
                justify="center"
                alignContent="center"
                style={{
                    overflow: 'hidden',
                    position: 'relative',
                    width: '100%',
                    height: '100%',
                }}
            >
                <Grid
                    item
                    container
                    alignContent="center"
                    direction="column"
                    justify="center"
                    style={{ textAlign: 'center', padding: '3rem 0 3rem 0' }}
                >
                    <Grid item>
                        <Typography variant="h6" color="textPrimary">
                            Sign Up
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography
                            variant="body1"
                            color="textPrimary"
                            style={{ padding: '0 0 2rem 0' }}
                        >
                            Join the Lusack Community
                        </Typography>
                    </Grid>

                    <form action="">
                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <OutlinedInput
                                required
                                id="standard-basic"
                                placeholder="Email"
                                fullWidth
                                name="email"
                                value={email}
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <OutlinedInput
                                required
                                id="standard-basic"
                                placeholder="Password"
                                type="password"
                                fullWidth
                                name="password"
                                value={password}
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <OutlinedInput
                                required
                                placeholder="Confirm Password"
                                type="password"
                                margin="none"
                                fullWidth
                                name="confirmPass"
                                value={confirmPass}
                                onChange={handleChange}
                            />
                        </Grid>

                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <Button
                                variant="contained"
                                color="primary"
                                fullWidth
                                onClick={() => {
                                    if (
                                        password === confirmPass &&
                                        password.length >= 8 &&
                                        check
                                    ) {
                                        app.auth()
                                            .createUserWithEmailAndPassword(
                                                email,
                                                password
                                            )
                                            .then(() => {
                                                Router.push('/');
                                                // PrivateRoute();
                                            })
                                            .catch(function () {
                                                // Handle Errors here.

                                                setState({
                                                    ...state,
                                                    error: true,
                                                });
                                                // console.log("errorCode= "+ errorCode);
                                                // console.log("errorMessage= "+ errorMessage);
                                                // ...
                                            });
                                    } else {
                                        // console.log("passwords do not match or password is too short");
                                        setState({ ...state, error: true });
                                    }
                                }}
                            >
                                Sign Up
                            </Button>
                        </Grid>
                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <Error
                                err={error}
                                message="Enter valid credentials. Passwords must be at least 8 characters long."
                            />
                        </Grid>

                        <Grid item style={{ padding: '0 0 .8rem 0' }}>
                            <FormControl component="fieldset">
                                <FormGroup aria-label="position" row>
                                    <FormControlLabel
                                        value="top"
                                        control={
                                            <Checkbox
                                                color="secondary"
                                                checked={check}
                                                onChange={handleCheckboxChange}
                                            />
                                        }
                                        label={
                                            <Typography variant="body2">
                                                I accept Lusack's{' '}
                                                <Link variant="body2" href="#">
                                                    {' '}
                                                    Terms Of Use{' '}
                                                </Link>{' '}
                                                and{' '}
                                                <Link variant="body2" href="#">
                                                    Privacy Policy
                                                </Link>
                                            </Typography>
                                        }
                                        labelPlacement="end"
                                    />
                                    <FormControlLabel
                                        value="top"
                                        control={
                                            <Checkbox
                                                color="secondary"
                                                checked={check2}
                                                onChange={handleCheckboxChange2}
                                            />
                                        }
                                        label={
                                            <Typography variant="body2">
                                                I would like email updates on
                                                the hottest deals
                                            </Typography>
                                        }
                                        labelPlacement="end"
                                    />
                                </FormGroup>
                            </FormControl>
                        </Grid>

                        <Grid
                            item
                            style={{
                                padding: '0 0 .8rem 0',
                                textAlign: 'center',
                            }}
                        >
                            <Typography variant="body1">
                                &#9473;&#9473;&#9473;&#9473;&#9473;&#9473;&#9473;&nbsp;
                                or&nbsp;&#9473;&#9473;&#9473;&#9473;&#9473;&#9473;&#9473;
                            </Typography>
                            <Typography variant="body1">
                                Sign up with
                            </Typography>
                        </Grid>
                        <Grid
                            item
                            container
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Grid item xs>
                                <Button
                                    onClick={() => {
                                        var provider = new app.auth.FacebookAuthProvider();

                                        app.auth()
                                            .signInWithPopup(provider)
                                            .then(function () {
                                                // This gives you a Google Access Token. You can use it to access the Google API.
                                                // var token = result.credential.accessToken;
                                                // The signed-in user info.
                                                // console.log("1"+ !!currentUser);

                                                // setCurrentUser(user);
                                                // console.log("2"+ !!currentUser);

                                                // PrivateRoute();
                                                Router.push('/');
                                                // ...
                                            })
                                            .catch(function () {
                                                // Handle Errors here.
                                                // The email of the user's account used.
                                                // The firebase.auth.AuthCredential type that was used.
                                                // ...
                                            });
                                    }}
                                >
                                    <FacebookIcon width="2rem" height="auto" />
                                </Button>
                            </Grid>
                            <Grid item xs>
                                <Button
                                    onClick={() => {
                                        var provider = new app.auth.GoogleAuthProvider();
                                        // console.log("1"+ !!currentUser);

                                        app.auth()
                                            .signInWithPopup(provider)
                                            .then(function () {
                                                // This gives you a Google Access Token. You can use it to access the Google API.
                                                // var token = result.credential.accessToken;
                                                // The signed-in user info.

                                                // setCurrentUser(user);
                                                Router.push('/');
                                                // PrivateRoute();
                                                // console.log("2"+ !!currentUser);

                                                // ...
                                            })
                                            .catch(function () {
                                                // Handle Errors here.
                                                // The email of the user's account used.
                                                // The firebase.auth.AuthCredential type that was used.
                                                // ...
                                            });
                                    }}
                                >
                                    <GoogleIcon width="2rem" height="auto" />
                                </Button>
                            </Grid>
                            <Grid item xs>
                                <Button disabled={true}>
                                    <TwitterIcon width="2rem" height="auto" />
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography
                                variant="body2"
                                style={{ padding: '3rem 0 3rem 0' }}
                            >
                                {'Already have an account? '}
                                <Link variant="body2" href="/login">
                                    Log in Here
                                </Link>
                            </Typography>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
        </div>
    );
}
