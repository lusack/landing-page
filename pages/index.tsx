import React, { ReactElement } from 'react';
import GiantLogo from '../components/GiantLogo';
import Hero from '../components/landing/Hero';
import Section1 from '../components/landing/Section1';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Section2 from '../components/landing/Section2';
import FAQ from '../components/landing/FAQ';

interface Props {}

export default function index({}: Props): ReactElement {
    const matches = useMediaQuery('(min-width:1920px)');
    const matches1 = useMediaQuery('(min-width:1500px)');
    const matches2 = useMediaQuery('(min-width:1440px)');
    const matches3 = useMediaQuery('(min-width:1280px)');
    const matches4 = useMediaQuery('(min-width:1152px)');
    function LandingBackground() {
        if (matches) {
            //1920 3000 2400 7413 9461
            return <GiantLogo xPos="2600" yPos="2400" />;
        } else if (matches1) {
            //1500
            return <GiantLogo xPos="2800" yPos="2400" />;
        } else if (matches2) {
            //1440
            return <GiantLogo xPos="2800" yPos="2400" />;
        } else if (matches3) {
            //1280
            return <GiantLogo xPos="3000" yPos="2900" />;
        } else if (matches4) {
            //1152
            return <GiantLogo xPos="3100" yPos="2400" />;
        } else {
            return <GiantLogo xPos="3100" yPos="2400" />;
        }
    }

    return (
        <div style={{ overflow: 'hidden', position: 'relative' }}>
            <div style={{ position: 'absolute', zIndex: 0 }}>
                <LandingBackground />
            </div>
            <Hero />
            <Section1 />
            <Section2 />
            <FAQ />
        </div>
    );
}
