import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

const white = '#F5F3F3';
const black = '#171E27';
const red = '#FF4957';

let theme = createMuiTheme({
    typography: {
        fontFamily: 'Lato',

        h1: {
            fontFamily: 'Bebas Neue',
        },
        h2: {
            fontFamily: 'Bebas Neue',
        },
        h3: {
            fontFamily: 'Bebas Neue',
        },
        h4: {
            fontFamily: 'Bebas Neue',
        },
        h5: {
            fontFamily: 'Montserrat',
            fontSize: '2rem',
        },
        h6: {
            fontFamily: 'Montserrat',
            fontSize: '3rem',
        },

        button: {
            fontFamily: 'Bebas Neue',
            fontSize: '1.5rem',
        },
        body1: {},
        body2: {
            fontFamily: 'Montserrat',
        },
    },

    palette: {
        type: 'dark',
        text: {
            primary: white,
            secondary: black,
            disabled: 'rgba(245, 243, 243, 0.5)',
        },
        primary: {
            // light: will be calculated from palette.primary.main,
            main: red,
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },

        secondary: {
            main: white,
            dark: white,

            // dark: will be calculated from palette.secondary.main,
            contrastText: black,
        },
        background: {
            default: black,
        },

        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
});
theme = responsiveFontSizes(theme);
theme.typography.h1 = {
    ...theme.typography.h1,
    '@media (max-width:1200px)': {
        fontSize: '4.5rem',
    },
};
export default theme;
