import { createContext } from 'react';

export interface IErrorContext {
    error: boolean;
}
const initialErrorContext: IErrorContext = {
    error: false,
};

export const ErrorContext = createContext<any>(initialErrorContext);
