import { createContext } from 'react';

export interface IFormContextProps {}

export interface IFormContext {
    parts: {
        gpu: boolean;
        cpu: boolean;
        ram: boolean;
        mobo: boolean;
        psu: boolean;
        storage: boolean;
        gpuModelNumber: string | null;
        cpuModelNumber: string | null;
        ramModelNumber: string | null;
        moboModelNumber: string | null;
        psuModelNumber: string | null;
        storageModelNumber: string | null;
    };
    budget: {
        lower: number;
        upper: number;
    };
    purpose: {
        gaming: boolean;
        schoolAndWork: boolean;
        cad: boolean;
        videoEditing: boolean;
        threeD: boolean;
        home: boolean;
    };
    preferences: {
        shopping: string;
    };
}
const initialAppContext: IFormContext = {
    parts: {
        gpu: false,
        cpu: false,
        ram: false,
        mobo: false,
        psu: false,
        storage: false,
        gpuModelNumber: '',
        cpuModelNumber: '',
        ramModelNumber: '',
        moboModelNumber: '',
        psuModelNumber: '',
        storageModelNumber: '',
    },
    budget: {
        lower: 0,
        upper: 0,
    },
    purpose: {
        gaming: false,
        schoolAndWork: false,
        cad: false,
        videoEditing: false,
        threeD: false,
        home: false,
    },
    preferences: {
        shopping: '',
    },
};

export const FormContext = createContext<any>(initialAppContext);
