import app from "./base";
import React, { useEffect, useState, ReactNode } from 'react'
export const AuthContext = React.createContext<any| firebase.User | null>("bad");
interface IProps {
    children: ReactNode;
}


export const AuthProvider = ({ children }: IProps) => { const [currentUser, setCurrentUser] = useState<any | firebase.User | null>("bad");
    // const [pending,setPending] = useState(false)
    useEffect(() => {
        app.auth().onAuthStateChanged((user)=>{
            if(user=== null){
                console.log("false");
            }
console.log(true);
            setCurrentUser(user);
            // setPending(true);
        });
    }, []);
    // if(pending){
        // return <>Loading...</>;
    // }
    return (
        <AuthContext.Provider
            value={{
                currentUser            }}
        >
            {children}
        </AuthContext.Provider>
    );
};

